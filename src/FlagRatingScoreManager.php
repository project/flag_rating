<?php

namespace Drupal\flag_rating;

use Drupal\flag\FlagInterface;
use Drupal\flag\Event\FlagEvents;
use Drupal\flag\Event\FlaggingEvent;
use Drupal\flag\Event\UnflaggingEvent;
use Drupal\flag\FlagServiceInterface;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class FlagRatingScoreManager.
 */
class FlagRatingScoreManager implements EventSubscriberInterface {

  /**
   * The flag service.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  protected $flagService;

  /**
   * Constructor.
   *
   * @param \Drupal\flag\FlagServiceInterface $flag_service
   *   The flag service.
   */
  public function __construct(FlagServiceInterface $flag_service) {
    $this->flagService = $flag_service;
  }

  /**
   * Gets the average score given to a given entities.
   * 
   * @param \Drupal\flag\FlagInterface $flag
   *   The flag for which to retrieve a flag score.
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to retrieve a flag score.
   * @param mixed $anonymous_included
   *   (optional) Force the use or no use of Anonymous rating when doing calculations. Default: FALSE.
   *
   * @return int
   *   The average score for this given flag and entity.
   */
  public function getScore(FlagInterface $flag, EntityInterface $entity, $anonymous_included = NULL) {
    
    // Plugin configuration takes 
    if (NULL == $anonymous_included) {
      $anonymous_included = $flag->getThirdPartySetting('flag_rating', 'score_anonymous_included', FALSE);
    }

    try {
      if ($score_field = $flag->getThirdPartySetting('flag_rating', 'score_field', NULL)) {
        // Get all votes.
        $total = 0;
        $count = 0;
        $flaggings = $this->flagService->getEntityFlaggings($flag, $entity);
        foreach ($flaggings as $vote) {
          if (!$vote->hasField($score_field)) {
            throw new \Exception('Unknown Flag field: ' . $score_field);
          } 
          
          // Skip anonymous votes by default.
          if ($vote->getOwnerId() == '0' && !$anonymous_included) {
            continue;
          }

          
          // Get scores.
          $rating = (int) $vote->get($score_field)->getString();
          $total = $total + $rating;
          $count++;
        }
        // Calculate average.
        return $count > 0 ? (float) ($total/$count) : NULL;
      }
      else {
        throw new \Exception('Misconfigured Score field for Flag : ' . $flag->id());
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('flag_rating')->error('Error while trying to get score for this flag: ' . $flag->id() . '<br>'. $e->getMessage());
    }
  }

  /**
   * @todo.
   */
  public function updateScore($event) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[FlagEvents::ENTITY_FLAGGED][] = ['updateScore', -100];
    $events[FlagEvents::ENTITY_UNFLAGGED][] = ['updateScore', -100];
    return $events;
  }

}

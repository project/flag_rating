<?php


namespace Drupal\flag_rating\Plugin\ActionLink;

use Drupal\Core\Url;
use Drupal\flag\FlagInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\flag\ActionLink\ActionLinkTypeBase;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Provides the AJAX Rating link type.
 *
 * This class is a kind of duplicate of the AJAXactionLink type, but modified to
 * provide rating functionality.
 *
 * @ActionLinkType(
 *   id = "ajax_rating",
 *   label = @Translation("AJAX Rating"),
 *   description = "An AJAX JavaScript request which send a number alongside the flagging."
 * )
 */
class AJAXactionLinkRating extends ActionLinkTypeBase {

  /**
   * Undocumented function
   *
   * @param FlagInterface $flag
   * @return array
   *    The icon as renderable array.
   */
  protected function getIcon(FlagInterface $flag) {
    $icon = [];

    if ($fid = $flag->getThirdPartySetting('flag_rating', 'action_icon', NULL)) {
      if ($file = \Drupal::service('entity_type.manager')->getStorage('file')->load((int) $fid)) {
        $violations = \Drupal::service('file.validator')->validate($file, ['FileExtension' => ['extensions' => 'svg']]);
        $count = $violations->count();
        if (
          $count == 0 &&
          $raw = file_get_contents($file->getFileUri())
        ) {
          $icon = [
            '#type' => 'inline_template',
            '#template' => '{{ svg|raw }}',
            '#context' => [
              'svg' => strip_tags($raw, '<svg></svg><path></path><g></g><polygon></polygon>')
            ]
          ];
        }
      }
      else if ($file = flag_rating_create_default_icon()) {
        $icon = [
          '#theme' => 'image',
          '#uri' => $file->getFileUri(),
        ];
      }
    }
    return $icon;
  }

  /**
   * {@inheritDoc}
   */
  protected function getUrl($action, FlagInterface $flag, EntityInterface $entity) {
    switch($action) {
      case 'flag':
        return Url::fromRoute('flag.action_link_flag', [
          'flag' => $flag->id(),
          'entity_id' => $entity->id(),
        ]);

      default:
        return Url::fromRoute('flag.action_link_unflag', [
          'flag' => $flag->id(),
          'entity_id' => $entity->id(),
        ]);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getAsFlagLink(FlagInterface $flag, EntityInterface $entity) {
    $render = [];
    $action = $this->getAction($flag, $entity);
    $access = $flag->actionAccess($action, $this->currentUser, $entity);

    if(!$access->isAllowed()) {
      return $render;
    }

    // Get default values.
    $score = (int) \Drupal::service('flag_rating.count')->getScore($flag, $entity);
    $min = (int) $flag->getThirdPartySetting('flag_rating', 'score_min', 0);
    $max = (int) $flag->getThirdPartySetting('flag_rating', 'score_max', 5);
    $render['#min'] = $min;
    $render['#max'] = $max;
    $render['#score'] = $score;

    // Generate links.
    $items = [];
    for ($i = $min; $i <= $max; $i++) {
      $url = $this->getUrl($action, $flag, $entity, $i);
      $url->setRouteParameter('rating', $i);
      $url->setRouteParameter('destination', $this->getDestination());
      $render['#items'][$i] = [
        '#theme'      => 'flag_rating_icon',
        '#flag'       => $flag,
        '#flaggable'  => $entity,
        '#href'       => $url->toString(),
        '#title'      => $flag->getLongText($action),
        '#icon'       => $this->getIcon($flag),
        '#label'      => $flag->getShortText($action),
        '#rate'       => $i,
        '#min'        => $min,
        '#max'        => $max,
        '#score'      => $score,
        '#access'     => $access->isAllowed(),
      ];

      unset($url);
    }


    // Use our custom theme and library.
    $render['#flag'] = $flag;
    $render['#flaggable'] = $entity;
    $render['#theme'] = 'flag_rating';
    $render['#attached']['library'][] = 'flag_rating/flag.rating';

    CacheableMetadata::createFromRenderArray($items)
      ->addCacheableDependency($access)
      ->applyTo($render);

    return $render;
  }

}

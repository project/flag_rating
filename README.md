# Flag Rating

- This module is an extension of the great Flag module.
  Only supports Drupal 8.

## How To Install

 - `composer require  drupal/flag_rating`
 - `drush en flag_rating`

##  How To Configure

 - Create a new Flag under `Admin > Structure > Flag`
 - Select the plugin type: `AJAX Rating Link`
 - Save the Flag and edit it again
 - Select the field which will holds the scores
 - Optional: Upload an icon
 - Save your changes
Now, place the Flag on your content as you would usually do with fields - under `Admin > Structure > Content Type > Article Manage Display`.

## How To Customize Output (icon + Twig + CSS)

- A default SVG icon star.svg is provided by default but you can upload a different icon (png, jpg, jpeg or svg) 
  for each different Flag.

- Also, there are two useful templates that you can even override with some suggestions:
    - `flag-rating.html.twig`
    - `flag-rating-icon.html.twig`

- Finally, you can override or extend the CSS in your `theme .info` file like this:

```
libraries-extend:
  flag_rating/flag.rating: 
    - yourtheme/yourlibraryname
```

## Maintainers

- Matthieu Scarset (matthieuscarset) - https://www.drupal.org/u/matthieuscarset
- Ullrich Neiss (slowflyer) - https://www.drupal.org/u/slowflyer

